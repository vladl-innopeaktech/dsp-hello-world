cmake_minimum_required(VERSION 3.4.1)

if(NOT DEFINED ENV{HEXAGON_SDK_ROOT})
    set(HEXAGON_SDK_ROOT "$ENV{HOME}/Qualcomm/Hexagon_SDK/3.5.1")
endif()

if(NOT DEFINED ENV{ANDROID_NDK_ROOT})
    set(ANDROID_SDK_ROOT "$ENV{HOME}/Android/Sdk/ndk/21.0.6113669")
endif()

if(CMAKE_HOST_WIN32)
	set(hostOS WinNT)
else()
	set(hostOS Ubuntu14)
endif()

include(${CMAKE_CURRENT_SOURCE_DIR}/../../dsp/libs/hexagon_vars.cmake)

include_directories(
    ${HEXAGON_SDK_ROOT}/incs/
    ${HEXAGON_SDK_ROOT}/incs/stddef/)

##############################
#
# buildIDL (<idlFile> <currentTaget>) 
# 
# This function will set up a custom_target to build <idlFile> using qaic
# IDL compiler. For foo.idl, it wll generate foo.h, foo_stub.c and
# foo_skel.c into ${CMAKE_CURRENT_BINARY_DIR} directory.  
#
# This function will also add the custom_target created as the dependency
# of <currentTarget>
#
##############################
function(buildIDL idlFile currentTarget)
    message("BUILD IDL ${currentTarget}")
	set(QIDLINC "")
	set(defaultIncs
		${HEXAGON_SDK_ROOT}/incs/
		${HEXAGON_SDK_ROOT}/incs/stddef/
		${HEXAGON_SDK_ROOT}/libs/common/rpcmem/inc
		${HEXAGON_SDK_ROOT}/libs/common/remote/ship/${ANDROID_V}/
		)
	foreach(path ${defaultIncs})
		set(QIDLINC ${QIDLINC} "-I${path}")
	endforeach(path)

	message(STATUS "QIDLINC:${QIDLINC}")

	get_filename_component(fileName ${idlFile} NAME_WE)

	set(cmdLineOptions -mdll -o ${CMAKE_CURRENT_BINARY_DIR} ${QIDLINC} ${idlFile})
	message(STATUS "QIDL CMDLine:${cmdLine}")

	add_custom_target( qidlTarget${fileName} 
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/
		COMMAND "${HEXAGON_SDK_ROOT}/tools/qaic/${hostOS}/qaic" ${cmdLineOptions} && sed -i "/__QAIC_STUB_EXPORT int __QAIC_STUB/ a '#pragma weak remote_session_control\\nif (remote_session_control) { struct remote_rpc_control_unsigned_module data; data.enable = 1; data.domain = CDSP_DOMAIN_ID; remote_session_control(DSPRPC_CONTROL_UNSIGNED_MODULE, (void*)&data, sizeof(data)); }'" ${CMAKE_CURRENT_BINARY_DIR}/${fileName}_stub.c
		BYPRODUCTS  ${CMAKE_CURRENT_BINARY_DIR}/${fileName}_skel.c
		${CMAKE_CURRENT_BINARY_DIR}/${fileName}_stub.c
		${CMAKE_CURRENT_BINARY_DIR}/${fileName}.h
		)

	message(STATUS
		"BYPRODUCTS:${CMAKE_CURRENT_BINARY_DIR}/${fileName}_skel.c
		${CMAKE_CURRENT_BINARY_DIR}/${fileName}_stub.c
		${CMAKE_CURRENT_BINARY_DIR}/${fileName}.h")


	add_dependencies(${currentTarget} qidlTarget${fileName})

	set_source_files_properties(
		${CMAKE_CURRENT_BINARY_DIR}/${fileName}_skel.c
		${CMAKE_CURRENT_BINARY_DIR}/${fileName}_stub.c
		${CMAKE_CURRENT_BINARY_DIR}/${fileName}.h
		PROPERTIES
		GENERATED TRUE
		)
endfunction()

##############################
#
# buildSkel (<currentTaget>) 
# 
# This function will set up a custom_target to build libfoo_skel.so
# from foo.h, foo_stub.c foo_skel.c into ${CMAKE_CURRENT_BINARY_DIR} diretory.  
#
# This function will also add the custom_target created as the dependency
# of <currentTarget>
#
##############################
function(buildSkel currentTarget)
    message("BUILD SKEL ${currentTarget}")

	add_custom_target( skelTarget${currentTarget} 
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/
        COMMAND  #TODO make these exports portable
            export HEXAGON_SDK_ROOT="${HEXAGON_SDK_ROOT}";
            export QNX_HOST=$HEXAGON_SDK_ROOT/tools/qnx/qnx_700/host/linux/x86_64/;
            export QNX_TARGET=$HEXAGON_SDK_ROOT/tools/qnx/qnx_700/target/qnx7/;
            export SDK_SETUP_ENV=Done;
            export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HEXAGON_SDK_ROOT/tools/libusb/;
            export DEFAULT_HEXAGON_TOOLS_ROOT=$HEXAGON_SDK_ROOT/tools/HEXAGON_Tools/8.3.07;
            export ANDROID_ROOT_DIR="${ANDROID_NDK_ROOT}";
            make tree_clean V=${HEXAGON_V} VERBOSE=1 && make tree V=${HEXAGON_V} VERBOSE=1 && cp ${CMAKE_CURRENT_SOURCE_DIR}/${HEXAGON_V}/ship/lib${currentTarget}_skel.so ${CMAKE_CURRENT_SOURCE_DIR}/src/main/assets/;
		 BYPRODUCTS  ${CMAKE_CURRENT_SOURCE_DIR}/${HEXAGON_V}/ship/lib${currentTarget}_skel.so
		)

	message(STATUS
		"BYPRODUCTS:${CMAKE_CURRENT_SOURCE_DIR}/${HEXAGON_V}/ship/lib${currentTarget}_skel.so")

	add_dependencies(${currentTarget} skelTarget${currentTarget})

endfunction()
